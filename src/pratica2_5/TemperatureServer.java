
import java.rmi.*;

public interface TemperatureServer extends Remote {
    public abstract String metodoServidor() throws RemoteException;
}//fim interface